---
title: team command
date: 2017-10-06
---
# Uitleg markdown (kop 1)

## Kop 2

### Kop 3

Dit zijn voorbeelden van tekst opmaak: _schuin_, *schuin*, __vet__, **vet**, `vaste breedte`.

Een paragraaf wordt gescheiden door een dubbele return.

## _Opsomming cijfers_
1. Dit 
2. Is 
3. Een
4. Opsomming 

## _Opsomming bullitpoints_
* Dit 
* Is
* Ook 
* Een
* Opsomming
* 

Horizontale lijn:

---

